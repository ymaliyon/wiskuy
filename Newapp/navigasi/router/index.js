import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import AboutAppScreen from '../pages/AboutApp';
import AboutMeScreen from '../pages/AboutMe';
import ContentScreen from '../pages/Content';
import DaftarScreen from '../pages/Daftar';
import HomeScreen from '../pages/Home';
import LoginScreen from '../pages/Login';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function router(){
    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false}}>
                <Stack.Screen name="DaftarScreen" component={DaftarScreen} />
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="HomeScreen" component={HomeScreen} />
                <Stack.Screen name="ContentScreen" component={ContentScreen} />
                <Stack.Screen name="MyDrawwer" component={MyDrawwer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MyDrawwer = () => (
    <Drawwer.Navigator screenOptions={{headerShown: false}}>
        <Drawwer.Screen name="Home" component={HomeScreen} />
        <Drawwer.Screen name="Tentang Saya" component={AboutMeScreen} />
        <Drawwer.Screen name="Tentang Aplikasi" component={AboutAppScreen} />
    </Drawwer.Navigator>
)