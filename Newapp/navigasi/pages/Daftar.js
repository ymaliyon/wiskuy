// import React from "react";
// import { StyleSheet, View, Text } from "react-native";

// export default function DaftarScreen({navigation}){
//     return(
//         <View style={styles.container}>
//             <Text>Daftar</Text>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center'
//     }
// })

import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import firebase from 'firebase'

export default function DaftarScreen({navigation}){
    const firebaseConfig = {
        apiKey: "AIzaSyDtBOHkVbK7sfg-ZighmV3eSIatSr3iYZU",
        authDomain: "wiskuy-211ef.firebaseapp.com",
        projectId: "wiskuy-211ef",
        storageBucket: "wiskuy-211ef.appspot.com",
        messagingSenderId: "421425134152",
        appId: "1:421425134152:web:b59f961191d002c135767d"
    };
    // Initialize Firebase
    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }

    const submit=()=>{
        const data = {
            email, password
        }
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(()=>{
            navigation.navigate("MyDrawwer")
        }).catch(()=>{
            alert("GAGAL MENDAFTAR\nMasukan email dan password dengan benar!");
        })
    }

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    return(
        <LinearGradient colors={['green', 'white']} style={styles.warnaApp}>
            <View style={styles.container}>
                <Text style={styles.judulApp}>Wiskuy</Text>
                <View style={styles.inputView}>
                    <TextInput 
                        style={styles.TextInput}
                        placeholder='Masukan Email'
                        placeholderTextColor='#003f5c'
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput 
                        style={styles.TextInput}
                        placeholder='Masukan Password'
                        placeholderTextColor='#003f5c'
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />
                </View>
                <Text style={{paddingTop: 5, alignSelf: 'flex-start', marginLeft: 49}}>Minimal memiliki 8 karakter</Text>
                <TouchableOpacity onPress={submit} style={styles.daftarBtn}>
                        <Text style={styles.daftarText}>Daftar</Text>
                </TouchableOpacity>               
                <Text style={styles.andaSudah}>Anda sudah memiliki akun?</Text>
                <View style={styles.masukBtn}>
                    <TouchableOpacity onPress={()=>navigation.navigate("LoginScreen")}>
                        <Text style={styles.masukText}>Masuk</Text>
                    </TouchableOpacity>
                </View>
                <StatusBar style='auto' />
            </View>
        </LinearGradient>        
    )
}

const styles = StyleSheet.create({
    warnaApp:{
        flex: 1,
        width: '100%'
    },
    container:{
        flex: 1,
        alignItems: 'center',
    },
    judulApp:{
        fontSize: 70,
        paddingTop: 150,
        paddingBottom: 50,
        fontWeight: 'bold'
    },
    inputView:{
        backgroundColor: '#F0FFFF',
        marginTop: 20,
        width: '80%',
        height: 40,
        justifyContent: 'center',
        borderRadius: 10
    },
    TextInput:{
        padding: 10
    },
    daftarBtn:{
        marginTop: 20,
        height: 40,
        width: 100,
        backgroundColor: 'blue',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    daftarText:{
        color: 'white',
        fontSize: 20
    },
    andaSudah:{
        marginTop: 10,
        alignSelf: 'flex-start',
        paddingLeft: 45,
        fontSize: 18
    },
    masukBtn:{
        alignSelf: 'flex-start',
        paddingLeft: 45
    },
    masukText:{
        color: 'blue',
        fontSize: 18
    }
})