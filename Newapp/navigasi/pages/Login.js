// import React from "react";
// import { StyleSheet, View, Text } from "react-native";

// export default function LoginScreen(){
//     return(
//         <View style={styles.container}>
//             <Text>Login</Text>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center'
//     }
// })

import { LinearGradient } from "expo-linear-gradient";
import React, { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import firebase from "firebase";

export default function LoginScreen({navigation}){
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const firebaseConfig = {
        apiKey: "AIzaSyDtBOHkVbK7sfg-ZighmV3eSIatSr3iYZU",
        authDomain: "wiskuy-211ef.firebaseapp.com",
        projectId: "wiskuy-211ef",
        storageBucket: "wiskuy-211ef.appspot.com",
        messagingSenderId: "421425134152",
        appId: "1:421425134152:web:b59f961191d002c135767d"
    };
    // Initialize Firebase
    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }

    const submit=()=>{
        const data={
            email, password
        }
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(()=>{
            navigation.navigate("MyDrawwer")
        }).catch(()=>{
            alert("LOGIN GAGAL\nPeriksa kembali email dan password!");
        })
    }

    return(
        <LinearGradient colors={['green', 'white']} style={styles.warnaApp}>
            <View style={styles.container}>
                <Text style={styles.judulApp}>Wiskuy</Text>
                <View style={styles.inputView}>
                    <TextInput 
                        style={styles.TextInput}
                        placeholder='Email'
                        placeholderTextColor='#003f5c'
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput 
                        style={styles.TextInput}
                        placeholder='Password'
                        placeholderTextColor='#003f5c'
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />
                </View>
                <TouchableOpacity style={styles.masukBtn} onPress={submit}>
                        <Text style={styles.masukText}>Masuk</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigation.navigate("DaftarScreen")}>
                    <Text style={styles.daftarText}>Belum Memiliki akun? Daftar disini</Text>
                </TouchableOpacity>
            </View>
        </LinearGradient>        
    )
}

const styles = StyleSheet.create({
    warnaApp:{
        flex: 1,
        width: '100%'
    },
    container:{
        flex: 1,
        alignItems: 'center',
    },
    judulApp:{
        fontSize: 70,
        paddingTop: 150,
        paddingBottom: 50,
        fontWeight: 'bold'
    },
    inputView:{
        backgroundColor: '#F0FFFF',
        marginTop: 20,
        width: '80%',
        height: 40,
        justifyContent: 'center',
        borderRadius: 10
    },
    TextInput:{
        padding: 10
    },
    masukBtn:{
        marginTop: 40,
        height: 40,
        width: 100,
        backgroundColor: 'blue',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    masukText:{
        color: 'white',
        fontSize: 20
    },
    daftarText:{
        marginTop: 10,
        color: 'blue',
        fontSize: 18
    }
})