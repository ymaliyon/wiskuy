// import React from "react";
// import { StyleSheet, View, Text } from "react-native";

// export default function AboutMeScreen(){
//     return(
//         <View style={styles.container}>
//             <Text>AboutMe</Text>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center'
//     }
// })

import { LinearGradient } from "expo-linear-gradient";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import { Ionicons, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'

export default function AboutMeScreen({navigation}){
    return (
        <LinearGradient colors={['green', 'white']} style={styles.warnaBackground}>
            <View style={styles.container}>
                <SafeAreaView>
                    <StatusBar style='auto' />
                </SafeAreaView>
                <View style={styles.topHeader}>
                    <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                        <Ionicons name='menu' size={30} />
                    </TouchableOpacity>
                    <Text style={styles.userNa}>Tentang Saya</Text>
                </View>
                <Image style={styles.image} source={require('../../assets/profil.jpg')} />
                <View style={styles.namaProfil}>
                    <Text style={{fontSize: 34, fontWeight: 'bold'}}>Yonna Maliyon</Text>
                    <Text style={{fontSize: 18}}>- React Native Developer -</Text>
                </View>
                <View style={styles.kotak}>
                    <View style={styles.header}>
                        <Text style={styles.judulHeader}>Portofolio</Text>
                    </View>
                    <View style={styles.isiPortofolio}>
                        <View style={styles.iconGitlab}>
                            <FontAwesome name='gitlab' size={50} color='black' />
                            <Text style={styles.akunAtas}>@ymaliyon</Text>
                        </View>
                        <View style={styles.iconGithub}>
                            <Ionicons name='logo-github' size={50} color='black' />
                            <Text style={styles.akunAtas}>@ymaliyon</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.kotak}>
                    <View style={styles.header}>
                        <Text style={styles.judulHeader}>Kontak</Text>
                    </View>
                    <View style={styles.isiKontak}>
                        <View style={styles.iconBawah}>
                            <Ionicons name='logo-whatsapp' size={40} color='black' />
                            <Text style={styles.akunBawah}>083824733197</Text>
                        </View>
                        <View style={styles.iconBawah}>
                            <Ionicons name='logo-instagram' size={40} color='black' />
                            <Text style={styles.akunBawah}>@ymaliyon</Text>
                        </View>
                        <View style={styles.iconBawah}>
                            <MaterialCommunityIcons name='gmail' size={40} color='black' />
                            <Text style={styles.akunBawah}>ymaliyon@gmail.com</Text>
                        </View>
                    </View>
                </View>
            </View>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    warnaBackground:{
        flex: 1,
        width: '100%'
    },
    container:{
        flex: 1,
        alignItems: 'center'
    },
    topHeader:{
        flexDirection: 'row',
        height: '5%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    userNa:{
        fontSize: 20
    },
    image:{
        height: 200,
        width: 200,
        borderRadius: 100,
        marginTop: 40
    },
    namaProfil:{
        marginTop: 10,
        marginBottom: 20,
        alignItems: 'center'
    },
    kotak:{
        marginTop: 20,
        borderWidth: 2,
        width: '80%'
    },
    header:{
        borderBottomWidth: 1
    },
    judulHeader:{
        alignSelf: 'center',
        fontSize: 24,
    },
    isiPortofolio:{
        margin: 5,
        flexDirection: 'row',
        padding: 5,
        justifyContent: 'space-evenly',
    },
    iconGitlab:{
        flexDirection: 'row',
        marginTop: 3
    },
    iconGithub:{
        flexDirection: 'row'
    },
    akunAtas:{
        alignSelf: 'center',
        paddingLeft: 8,
        fontSize: 18
    },
    isiKontak:{
        margin: 5,
        paddingLeft: 5
    },
    iconBawah:{
        flexDirection: 'row'
    },
    akunBawah:{
        alignSelf: 'center',
        paddingLeft: 10,
        fontSize: 18
    }
})