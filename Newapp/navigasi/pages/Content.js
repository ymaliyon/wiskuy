// import React from "react";
// import { StyleSheet, View, Text } from "react-native";

// export default function ContentScreen({route, navigation}){
//     const {itemImage, itemNama, itemLokasi, itemTiket, itemDesc} =route.params
//     return(
//         <View style={styles.container}>
//             <Text>{itemNama}</Text>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center'
//     }
// })

import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { StatusBar } from "expo-status-bar";
import { Ionicons } from '@expo/vector-icons'

export default function ContentScreen({route, navigation}){
    const {itemImage, itemNama, itemLokasi, itemTiket, itemDesc} = route.params
    return(
        <LinearGradient colors={['green', 'white']} style={styles.warnaBackground}>
            <SafeAreaView>
                <StatusBar style='auto' />
            </SafeAreaView>
            <View style={styles.topHeader}>
                <TouchableOpacity onPress={()=>navigation.navigate("MyDrawwer")}>
                    <Ionicons name='arrow-back' size={30} />
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.container}>
                <Image
                    style={styles.gambar}
                    source={itemImage}
                />
                <Text style={styles.judul}>{itemNama}</Text>
                <View style={styles.isiAtas}>
                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>Lokasi :</Text>
                    <Text style={{fontSize:18}}>{itemLokasi}</Text>
                </View>
                <View style={styles.isiAtas}>
                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>Harga Tiket :</Text>
                    <Text style={{fontSize: 18}}>{itemTiket}</Text>
                </View>
                <Text style={{fontSize: 18, fontWeight: 'bold', alignSelf: 'center'}}>Deskripsi</Text>
                <Text style={styles.isiBawah}>{itemDesc}</Text>
            </ScrollView>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    warnaBackground:{
        flex: 1,
        width: '100%'
    },
    container:{
    },
    topHeader:{
        flexDirection: 'row',
        height: '5%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: '5%'
    },
    gambar:{
        marginTop: 10,
        height: 250,
        width: '90%',
        borderRadius: 10,
        alignSelf: 'center'
    },
    judul:{
        marginTop: 10,
        fontSize: 32,
        fontWeight: 'bold',
        alignSelf: 'center',
        textAlign: 'center'
    },
    lokasi:{
        marginTop: 10,
        alignSelf: 'flex-start',
        paddingLeft: '5%',
        fontSize: 18
    },
    isiAtas:{
        marginTop: 5,
        alignSelf: 'flex-start',
        paddingLeft: '5%',
        paddingRight: '5%',
        marginBottom: 5,
    },
    isiBawah:{
        width: '90%',
        textAlign: 'justify',
        alignSelf: 'center',
        fontSize: 18,
    }
})