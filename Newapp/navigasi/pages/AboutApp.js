// import React from "react";
// import { StyleSheet, View, Text } from "react-native";

// export default function AboutAppScreen(){
//     return(
//         <View style={styles.container}>
//             <Text>AboutApp</Text>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center'
//     }
// })

import { LinearGradient } from "expo-linear-gradient";
import { SafeAreaView } from "react-native-safe-area-context"
import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Ionicons } from '@expo/vector-icons'

export default function AboutAppScreen ({navigation}){
    return(
        <LinearGradient colors={['green', 'white']} style={styles.warnaBackground}>
            <View style={styles.container}>
                <SafeAreaView>
                    <StatusBar style='auto' />
                </SafeAreaView>
                <View style={styles.topHeader}>
                    <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                        <Ionicons name='menu' size={30} />
                    </TouchableOpacity>
                    <Text style={styles.userNa}>Tentang Aplikasi</Text>
                </View>
                <Text style={styles.judul}>Wiskuy</Text>
                <View style={styles.kotak}>
                    <Text style={styles.tulisan}>
                        Wiskuy atau singkatan dari "Wisata Kuy", adalah project perdana saya sekaligus sebagai Final Project dari Jabar Coding Camp 2021 yang saya ikuti. Maka dari itu mohon maaf jika terdapat banyak kekurangan yang ada pada aplikasi ini.
                    </Text>
                    <Text style={styles.tulisan}>
                        Aplikasi ini berisikan info-info tempat wisata yang ada didaerah Jawa Barat, seperti nama wisatanya, penjelasan, lokasi, dll. Yang dikumpulkan dari berbagai sumber. Walaupun tidak sedetail dengan keadaan aslinya karena keterbatasan yang saya miliki.
                    </Text>
                    <Text style={styles.tulisan}>
                        Untuk kritik dan saran silahkan menghubungi saya melalui kontak yang tersedia di "About Me". Mudah-mudahan project ini bisa terus dilanjutkan dan menjadi lebih baik lagi. Terimakasih
                    </Text>
                </View>
            </View>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    warnaBackground:{
        flex: 1,
        width: '100%'
    },
    container:{
        flex: 1,
        alignItems: 'center'
    },
    topHeader:{
        flexDirection: 'row',
        height: '5%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    userNa:{
        fontSize: 20
    },
    judul:{
        marginTop: 40,
        fontSize: 64
    },
    kotak:{
        marginTop: 30,
        width: '85%',
        borderWidth: 1,
        borderRadius: 10
    },
    tulisan:{
        fontSize: 24,
        textAlign: 'justify',
        justifyContent: 'space-evenly',
        margin: 7
    }
})