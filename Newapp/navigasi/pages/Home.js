// import React from "react";
// import { StyleSheet, View, Text } from "react-native";

// export default function HomeScreen(){
//     return(
//         <View style={styles.container}>
//             <Text>Home</Text>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center'
//     }
// })

import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useEffect } from "react";
import { Button, StyleSheet, Text, View, Image, SectionList } from "react-native";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons } from '@expo/vector-icons'
import { SafeAreaView } from "react-native-safe-area-context";
import { Data } from '../data/IsiContent'
import firebase from "firebase";

export default function HomeScreen({route, navigation}){
    useEffect(()=>{
        console.log(Data);
    }, [])

    const Logout=()=>{
        firebase.auth().signOut()
        .then(()=>{
            navigation.navigate("LoginScreen")
        })
    }

    return(
        <LinearGradient colors={['green', 'white']} style={styles.warnaBackground}>
            <View style={styles.container}>
                <SafeAreaView>
                    <StatusBar style='auto' />
                </SafeAreaView>
                <View style={styles.topHeader}>
                    <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                        <Ionicons name='menu' size={30} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={Logout}>
                        <Text style={styles.userNa}>Logout</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>
                    <FlatList
                        data={Data}
                        keyExtractor={(item)=>item.id}
                        renderItem={({item})=>{
                            return(
                                <TouchableOpacity onPress={()=>navigation.navigate("ContentScreen", {itemImage: item.image, itemNama: item.nama, itemLokasi: item.lokasi, itemTiket: item.tiket, itemDesc: item.desc})} style={styles.isiContent}>
                                    <Text style={styles.judulContent}>{item.nama}</Text>
                                    <Image
                                        style={{height: 100, width: 145, borderRadius: 10}}
                                        source={item.image}
                                    />
                                    <Text style={{fontSize: 18, paddingVertical: 1}}>{item.kota}</Text>
                                </TouchableOpacity>
                            )
                        }}
                        numColumns={2}
                    />
                </View>
            </View>
        </LinearGradient>
    )
}

const styles = StyleSheet.create({
    warnaBackground:{
        flex: 1,
        width: '100%'
    },
    container:{
        flex: 1,
        alignItems: 'center'
    },
    topHeader:{
        flexDirection: 'row',
        height: '5%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    userNa:{
        fontSize: 20
    },
    content:{
        alignItems: 'center',
    },
    isiContent:{
        alignItems: 'center',
        justifyContent: 'space-evenly',
        borderWidth: 1,
        margin: 5,
        width: 180,
        height: 160,
        borderRadius: 10,
        backgroundColor: '#32CD32'
    },
    judulContent:{
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        height: 30,
        textAlignVertical: 'center'
    }
})