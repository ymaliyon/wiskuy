import React from 'react';
import 'react-native-gesture-handler';
import { Settings, StyleSheet, Text, View } from 'react-native';
import Awal from './navigasi/index';

export default function App() {
  return (
    <Awal />
  );
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  }
})